<?php

namespace App\Http\Controllers;

class NumbersController extends Controller
{
    /**
     * Fungsi untuk mengambil bilangan fibonacci ke $n
     *
     * Contoh
     *   - n = 4, Fb(4) = 3
     */
    public function fibonacci($n = 5)
    {
        $fibos = [];
        for ($i = 0; $i <= $n; $i++) {
            if ($i == 0) {
                $fibos[$i] = 0;
            } elseif ($i == 1) {
                $fibos[$i] = 1;
            } else {
                $fibos[$i] = $fibos[$i-1] + $fibos[$i-2];
            }
        }

        return [
            'result' => $fibos[$n],
        ];
    }

    /**
     * buatlah fungsi penjumlahan 2 bilangan dari deret fibonacci
     *
     * - ambil bilangan fibonacci ke $n1
     * - ambil bilangan fibonacci ke $n2
     * - jumlahkan bilangan tersebut
     * - kembalikan hasilnya
     * - contoh
     *     - n1 = 1, Fb(1) = 1
     *     - n2 = 4, Fb(4) = 3
     *     - Fb(1) + Fb(4) = 4
     */
    public function fibonacciProduct($n1, $n2)
    {
        $fibo1 = $this->fibonacci($n1)['result'];
        $fibo2 = $this->fibonacci($n2)['result'];
        $totalfibonacci = $fibo1 + $fibo2;

        return [
            'result' => $totalfibonacci,
        ];
    }
}
